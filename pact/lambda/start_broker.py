import boto3
import os

def handler(event, context):
    ecs = boto3.client('ecs')
    cluster_name = os.environ['ECS_CLUSTER_NAME']
    service_name = os.environ['ECS_SERVICE_NAME']

    # Check the current number of tasks running for the service
    response = ecs.describe_services(
        cluster=cluster_name,
        services=[service_name]
    )
    service = response['services'][0]
    desired_count = service['desiredCount']

    # If the service is not running, set desired count to 1 to start it
    if desired_count == 0:
        ecs.update_service(
            cluster=cluster_name,
            service=service_name,
            desiredCount=1
        )
        print(f'Started ECS service: {service_name} on cluster: {cluster_name}')
    else:
        print(f'ECS service: {service_name} is already running on cluster: {cluster_name}')
