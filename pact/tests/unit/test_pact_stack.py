import aws_cdk as core
import aws_cdk.assertions as assertions

from pact.pact_stack import PactStack

# example tests. To run these tests, uncomment this file along with the example
# resource in pact/pact_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = PactStack(app, "pact")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
