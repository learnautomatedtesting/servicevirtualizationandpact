from aws_cdk import (
    aws_ecs as ecs,
    aws_ec2 as ec2,
    aws_ecs_patterns as ecs_patterns,
    aws_servicediscovery as sd,
    aws_s3 as s3,
    aws_lambda as _lambda,
    aws_iam as iam,
    aws_s3_notifications as s3_notifications,
    aws_logs as logs,
    aws_elasticloadbalancingv2 as elbv2,
    Stack,
    Duration,
    CfnOutput,
    RemovalPolicy
)
from constructs import Construct

class PactStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # Define a VPC
        vpc = ec2.Vpc(self, "Vpc", max_azs=2)

        # Define an ECS cluster
        cluster = ecs.Cluster(self, "PactBrokerCluster", vpc=vpc)
        
        # Create a CloudWatch Log Group
        log_group = logs.LogGroup(self, "PactBrokerLogGroup",
                                  log_group_name="/ecs/pactbroker",
                                  removal_policy=RemovalPolicy.DESTROY)
        
         # Example: Define the security group for the Pact Broker ECS service
        postgres_sg = ec2.SecurityGroup(
            self, "PostgresSG",
            vpc=vpc,
            description="Allow PostgreSQL traffic",
            security_group_name="PostgresSecurityGroup"
        )

        # Example: Define or look up the security group for the PostgreSQL database
        # This example assumes an existing security group. If creating a new RDS instance,
        # you would create a new security group similarly to the Pact Broker SG.
        # Security Group for Pact Broker
        pact_broker_sg = ec2.SecurityGroup(
            self, "PactBrokerSG",
            vpc=vpc,
            description="Allow Pact Broker traffic",
            security_group_name="PactBrokerSecurityGroup"
        )

        # Add an ingress rule to the PostgreSQL security group to allow traffic
        # on port 5432 from the Pact Broker security group
        postgres_sg.add_ingress_rule(
            peer=pact_broker_sg,
             connection=ec2.Port.all_traffic(),
            description="Allow PostgreSQL traffic from Pact Broker"
        )
        
                # Allow Pact Broker to connect to PostgreSQL on port 5432
        postgres_sg.add_ingress_rule(
            peer=pact_broker_sg,
             connection=ec2.Port.all_traffic(),
            description="Allow Pact Broker access to PostgreSQL"
        )
        
        postgres_sg.add_egress_rule(peer=ec2.Peer.any_ipv4(), connection=ec2.Port.all_traffic(), description="Allow all outbound traffic")
        pact_broker_sg.add_egress_rule(peer=ec2.Peer.any_ipv4(), connection=ec2.Port.all_traffic(), description="Allow all outbound traffic")

        # Create a Cloud Map namespace for service discovery
        cloud_map_namespace = sd.PrivateDnsNamespace(
            self, "Namespace",
            name="pactbroker.local",
            vpc=vpc
        )
                
                # Create the task execution role
        task_execution_role = iam.Role(self, "PactBrokerTaskExecutionRole",
            assumed_by=iam.ServicePrincipal("ecs-tasks.amazonaws.com"),
            managed_policies=[
                iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AmazonECSTaskExecutionRolePolicy"),
            ]
        )

        # Add ECR permissions to the role
        task_execution_role.add_to_policy(iam.PolicyStatement(
            effect=iam.Effect.ALLOW,
            actions=[
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage"
            ],
            resources=["*"]
        ))


        # ECS Task Definition for PostgreSQL
        # ECS Task Definition for PostgreSQL
        postgres_task_definition = ecs.FargateTaskDefinition(self, "PostgresTask",
            execution_role=task_execution_role  # This is correct
        )
        postgres_container = postgres_task_definition.add_container(
            "PostgresContainer",
            image=ecs.ContainerImage.from_registry("282333594418.dkr.ecr.eu-west-2.amazonaws.com/my-postgres-repo:latest"),
            environment={
                "POSTGRES_DB": "pact_broker_db",
                "POSTGRES_USER": "pact_user",
                "POSTGRES_PASSWORD": "pact_password"
            },
            logging=ecs.LogDrivers.aws_logs(stream_prefix="postgres", log_group=log_group),
           
        )
        postgres_container.add_port_mappings(ecs.PortMapping(container_port=5432))

        # ECS Service for PostgreSQL with Cloud Map
        postgres_service = ecs.FargateService(self, "PostgresService",
            cluster=cluster,
            task_definition=postgres_task_definition,
            cloud_map_options=ecs.CloudMapOptions(
                name="postgres",
                cloud_map_namespace=cloud_map_namespace,
                container_port= 5432
            )
        )

            # ECS Fargate Service for Pact Broker
        task_definition = ecs.FargateTaskDefinition(self, "PactBrokerTask", execution_role=task_execution_role)
        container = task_definition.add_container("PactBroker",
            image=ecs.ContainerImage.from_registry("282333594418.dkr.ecr.eu-west-2.amazonaws.com/my-pactbroker-repo:latest"),
            environment={
                "PACT_BROKER_DATABASE_URL": "postgres://pact_user:pact_password@postgres.pactbroker.local/pact_broker_db",
                "PACT_BROKER_BASIC_AUTH_USERNAME": "admin",
                "PACT_BROKER_BASIC_AUTH_PASSWORD": "password",
                "PACT_BROKER_PUMA_PERSISTENT_TIMEOUT": "14"
            },
            logging=ecs.LogDrivers.aws_logs(stream_prefix="pactbroker", log_group=log_group),
        )
        # Ensure container listens on port 9292
        container.add_port_mappings(ecs.PortMapping(container_port=9292))

                # Customize the health check in ApplicationLoadBalancedFargateService to use the Heartbeat URL
        fargate_service = ecs_patterns.ApplicationLoadBalancedFargateService(self, "PactBrokerService",
            cluster=cluster,
            task_definition=task_definition,
            desired_count=1,  # Update desired_count to 2 to run two tasks
            public_load_balancer=True,
            cloud_map_options=ecs.CloudMapOptions(
                name="pactbroker",
                cloud_map_namespace=cloud_map_namespace,
                container_port= 9292
            ),
               security_groups=[pact_broker_sg,postgres_sg],  # Assigns the security group to the task
              assign_public_ip=False,  # For
        
        )

        # Update the health check configuration to use the heartbeat path
        fargate_service.target_group.configure_health_check(
            path="/diagnostic/status/heartbeat",
            interval=Duration.seconds(30),
            timeout=Duration.seconds(5),
            healthy_threshold_count=2,
            unhealthy_threshold_count=3,
            protocol=elbv2.Protocol.HTTP,
            healthy_http_codes="200-399"
        )
            

        # S3 Bucket for Pacts
        pacts_bucket = s3.Bucket(self, "PactsBucket", removal_policy=RemovalPolicy.DESTROY)

        # IAM Role for Lambda Function
        lambda_role = iam.Role(self, "LambdaExecutionRole",
                               assumed_by=iam.ServicePrincipal("lambda.amazonaws.com"),
                               managed_policies=[
                                   iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole"),
                                   iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaVPCAccessExecutionRole"),
                               ])

        # Add custom policy to lambda_role to allow it to update ECS service
        lambda_role.add_to_policy(iam.PolicyStatement(
            actions=["ecs:UpdateService"],
            resources=[fargate_service.service.service_arn],
        ))

        # Lambda Function to Start ECS Service
        lambda_function = _lambda.Function(self, "StartPactBroker",
                                           runtime=_lambda.Runtime.PYTHON_3_8,
                                           handler="start_pact_broker.handler",
                                           code=_lambda.Code.from_asset("lambda"),
                                           environment={
                                               "ECS_CLUSTER_NAME": cluster.cluster_name,
                                               "ECS_SERVICE_NAME": fargate_service.service.service_name,
                                           },
                                           role=lambda_role,
                                           )

        # S3 Event Notification for Lambda
        pacts_bucket.add_event_notification(s3.EventType.OBJECT_CREATED, s3_notifications.LambdaDestination(lambda_function))
        pacts_bucket.add_event_notification(s3.EventType.OBJECT_REMOVED, s3_notifications.LambdaDestination(lambda_function))

        # Output the ALB URL
        CfnOutput(self, "PactBrokerUrl", value=fargate_service.load_balancer.load_balancer_dns_name)
