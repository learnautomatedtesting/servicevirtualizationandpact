# First of all
## Open Source License

This project is open source and available under the [MIT License](LICENSE).

### Summary of the License

This license allows you to:

- **Use** the software for commercial and private purposes
- **Modify** the software to suit your needs
- **Distribute** the software to anyone and in any format
- **Sub-license**, and use the software as part of your own open source or proprietary project

Under the following conditions:

- **Attribution** - You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- **No Warranty** - The software is provided "as is", without warranty of any kind.

### Contributing

We welcome contributions from the community! If you wish to contribute, fork the repository, make your changes, and submit a pull request. For more details, see the [CONTRIBUTING.md](CONTRIBUTING.md) file.

### Questions and Support

For questions and support, please open an issue in the repository, and we will do our best to assist you.

---

This summary is not a substitute for the full license text. For the full legal terms and conditions, please see the [LICENSE](LICENSE) file in the repository.


# Now the instructional part


# PactStack on AWS Fargate with CDK

## Overview

PactStack is a serverless solution for running a Pact broker alongside a PostgreSQL database entirely on AWS Fargate, leveraging the AWS Cloud Development Kit (CDK) for infrastructure as code. This solution is designed to facilitate contract testing between microservices by providing a centralized broker for storing and retrieving Pact contracts. The stack includes an ECS cluster, Fargate services for the Pact broker and PostgreSQL database, a CloudWatch Log Group for logs, and an S3 bucket for storing Pacts.

## Architecture

- **VPC:** A custom VPC to securely host the resources.
- **ECS Cluster:** Hosts the Fargate services for the Pact broker and PostgreSQL.
- **Pact Broker & PostgreSQL Services:** Run in Fargate for scalability and serverless benefits.
- **CloudWatch Log Group:** Centralizes logs from the services.
- **S3 Bucket:** Stores Pact contracts and integrates with Lambda for event-driven updates.
- **API Gateway & Lambda:** (Optional) For additional integrations or functionality.

## Prerequisites

- AWS Account
- AWS CLI configured
- Python installed and your favourite IDE
- AWS CDK Toolkit

## Deployment

1. Clone this repository.
2. Navigate to the project directory and run `npm install` to install dependencies.
3. Customize the CDK stack as needed in `pact/pact/pact_stack`.
4. Deploy the stack to your AWS account with your credentials


## Note: in my gitlab-ci I created pipelines to cdk synth, deploy and destroy your stack. Make sure to enter your credentials in AWS secretsmanager or as IAM or other variables in the settings of gitlab ci

Pitfalls, if it is not running as expected check the security groups(so e.g. open the postgres port inbound from Pactbroker ), useally that can be a small issue why the cloudmapping is not working, and can make you spend one day chasing :-) 


   ```bash
   cdk deploy
