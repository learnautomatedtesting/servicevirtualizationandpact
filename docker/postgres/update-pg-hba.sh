#!/bin/bash
# Custom initialization script to update pg_hba.conf and postgresql.conf

# Append the desired line to pg_hba.conf
echo "host all all 0.0.0.0/0 trust" >> "$PGDATA/pg_hba.conf"

# Update listen_addresses in postgresql.conf
sed -ri "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PGDATA/postgresql.conf"

echo "Updated pg_hba.conf:"
cat "$PGDATA/pg_hba.conf"

echo "Updated postgresql.conf:"
grep "listen_addresses" "$PGDATA/postgresql.conf"